<?php
session_start();

header("Content-type: text/xml");

include "crypto.php";
include "utils.php";
include "settings.php";

if ($_SESSION["id"]){
switch ($_GET["action"]) {
    case 'ServerAction':
        $config = '<config><item Id="1" Parameter="AccessRoleFlags" Value="0" Type="number" /><item Id="3" Parameter="IsPreloaderFast" Value="0" Type="bool" /><item Id="4" Parameter="InitialVolumeValue" Value="30" Type="number" /><item Id="6" Parameter="IsPreloaderEnabled" Value="1" Type="bool" /><item Id="7" Parameter="IsStartupHomeLocation" Value="0" Type="bool" /><item Id="8" Parameter="SwfVersion" Value="" Type="string" /><item Id="9" Parameter="SynchronizeAvatarRotation" Value="1" Type="bool" /><item Id="10" Parameter="StatisticsSendInterval" Value="0" Type="number" /><item Id="12" Parameter="LanguageId" Value="1" Type="number" /><item Id="13" Parameter="SnId" Value="1" Type="number" /><item Id="14" Parameter="IsInternational" Value="0" Type="bool" /><item Id="15" Parameter="AutoServerSelectionAllowed" Value="1" Type="bool" /><item Id="16" Parameter="DaysToFullSoil" Value="28" Type="number" /><item Id="17" Parameter="DaysToHalfSoil" Value="14" Type="number" /><item Id="18" Parameter="CurrentQuest" Value="421" Type="number" /><item Id="20" Parameter="TypeWeapon" Value="3" Type="number" /><item Id="21" Parameter="SkipTutorial" Value="1" Type="bool" /><item Id="23" Parameter="CurrentQuestGroup" Value="6161" Type="string" /><item Id="24" Parameter="IsNewRegistration" Value="1" Type="bool" /><item Id="25" Parameter="IsMotivatingAdsOn" Value="0" Type="bool" /></config>';
        
        if (checkthem($gameServer_ip, $gameServer_port)){
            $serversList = '<servers><item Id="1" TRId="'.$gameServer_name.'" RId="5" RTMPUrl="rtmp://'.$gameServer_ip.':'.$gameServer_port.'/'.$gameServer_appname.'" Load="46" QuestLocationLoad="0" FriendsCount="0" ClubsCount="4" Weight="5.1" /></servers>';
        } else {
            $serverList = "<servers/>";
        }
                
        // TODO: Implement real auth
        $userData = '<user UserId="1" hwId="'. getGUID() .'" ticketId="'. getGUID() .'" RoleFlags="2" />';
        
        $system = '<system ServerDate="'.date('Y-m-d H:i:s').'" RPath="fs/3p897j5lf4e0j.swf" RVersion="56" />';
        
        echo '<?xml version="1.0" encoding="utf-8"?>
<response>
    <promotion>
        <i MRId="20106" State="1" />
        <i MRId="30896" State="7" />
    </promotion>
    <promotion_banner>
        <i MRId="30896" />
    </promotion_banner>
    <promotion_whats_new>
        <i Id="386" TypeId="1" MRId="20143" />
        <i Id="611" TypeId="2" MRId="27179" />
        <i Id="784" TypeId="2" MRId="30841" />
        <i Id="785" TypeId="2" MRId="30895" />
        <i Id="786" TypeId="2" MRId="30897" />
    </promotion_whats_new>
    <preloader>
        <i MRId="30894" ShowTime="20;00" />
    </preloader>
    <sn_status IsBinded="0" />
    <phone>
        <messages/>
    </phone>
    <user_name Value="test" />
    <postcard>
        <messages/>
    </postcard>
    <licence_promotion>
        <item Id="1" GroupId="0" OrderId="0" MRId="14763" />
        <item Id="2" GroupId="0" OrderId="1" MRId="14764" />
        <item Id="3" GroupId="0" OrderId="2" MRId="14765" />
        <item Id="4" GroupId="1" OrderId="0" MRId="14766" />
        <item Id="5" GroupId="1" OrderId="1" MRId="14767" />
        <item Id="6" GroupId="1" OrderId="2" MRId="14768" />
        <item Id="7" GroupId="2" OrderId="0" MRId="20597" />
        <item Id="8" GroupId="2" OrderId="1" MRId="20598" />
        <item Id="9" GroupId="2" OrderId="2" MRId="20599" />
    </licence_promotion>
    <flags EntranceCount="2600" />
    <tutorial>
        <item Id="-1" State="1" />
        <item Id="1" State="1" />
        <item Id="2" State="1" />
        <item Id="3" State="1" />
        <item Id="4" State="1" />
        <item Id="5" State="1" />
    </tutorial>
    <miniquest>
        <i Id="148515433" MiniquestId="90004" IsPostponed="0" StartDate="2018-06-06T18:58:28.113">
            <i Id="298265808" TaskId="900041" IsFinished="0" Counter="3" />
            <i Id="298265809" TaskId="900042" IsFinished="0" Counter="0" />
            <i Id="298265810" TaskId="900043" IsFinished="1" Counter="0" />
        </i>
    </miniquest>
    <grants ReceivingCount="0" />
    <requests ReceivingCount="0" />
    <cdata value="'. encrypt($config) .'" />
    <cdata value="'. encrypt($system) .'" />
    <cdata value="'. encrypt($userData) .'" />
    <cdata value="'. encrypt($serversList) .'" />
</response>';
        break;
    case 'Ping':
        echo '<response isPong="true" />';
        break;
    // add more below
    default:
        echo "there's nothing";
        break;
}
} else {
    echo "<!-- войди -->";
}
?>